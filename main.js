var n1, n2, result; // First Operand; Second Operand; Result; İlk sayı; İkinci Sayı; Sonuç;
var operand = 0; var operator; // Operand Seperator; Operator variable;

document.querySelector('#numbers').addEventListener('click', (event) => {
  var data = event.target.getAttribute('data-key');
  if (Number.isInteger(parseInt(data))) { // Number checking
    document.querySelector('.calc-number').innerText += event.target.getAttribute('data-key');
    if (operand == 1) n2 = document.querySelector('.calc-number').innerText; // Second operand
    else n1 = document.querySelector('.calc-number').innerText; // First operand
  } else { // If clicked any operator
    operand = 1;
    if (data == '+') operator = '+';
    if (data == '/') operator = '/';
    if (data == '*') operator = '*';
    if (data == '-') operator = '-';
    if (data == '=') {
      result = results(parseInt(n1), parseInt(n2), operator); // if i set as 'result', it is returning 'result is not a func' in second try. ask this!
      operand = 0;
      document.querySelector('.calc-number').innerText = n1 + operator + n2;
      document.querySelector('.result').innerText = result;
    } if (data == 'C') {
      n1 = 0; n2 = 0; result = 0; operand = 0;
      document.querySelector('.result').innerText = '';
    }
    if (data != '=') document.querySelector('.calc-number').innerText = ''; // Saves result data
  }
});

var results = (n1, n2, operator) => { // Calculator func
  switch (operator) {
    case '+':
      return n1 + n2;
    case '/':
      return n1 % n2 == 0 ? (n1 / n2) : (n1 / n2).toFixed(2); // if mod != 0 returns decimal
    case '*':
      return n1 * n2;
    case '-':
      return n1 - n2;
    default:
      break;
  }
}